#Analizador de PL0 Usando Jison

![Icon](http://velneo.es/files/2012/10/logo-js.png)

Este analizador trata de parsear con la gramática [PL/0][PL0].

Una vez introducido un programa (véase la sintaxis y la gramática que acepta [aquí][grammar]) el programa produce un árbol recursivo descendente.

También se incluye en la página un enlace a tests que prueban la completa funcionalidad de la página.

####Enlace a la web

[http://gentle-gorge-3583.herokuapp.com/][page]

####Autores

* Alejandro Hernández Hernández ([GitHub][Ale])
* Eliasib J. García Martín ([GitHub][Eli])

####Licencia

MIT

[PL0]: http://en.wikipedia.org/wiki/PL/0
[page]: http://gentle-gorge-3583.herokuapp.com/
[Ale]: https://github.com/alu0100699715
[Eli]: https://github.com/alu0100698121
[grammar]: http://gentle-gorge-3583.herokuapp.com/grammar
